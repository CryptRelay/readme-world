# Setting up ProtonVPN on a Raspberry Pi as a routed AP

At this moment, the [the official ProtonVPN Linux beta](https://protonvpn.com/support/official-linux-client/) is not available at the default OS of Raspberry Pis \(Raspbian\), hence we'll be using the [the community Linux app](https://protonvpn.com/support/linux-vpn-tool/). The idea of future of this guide will be to migrate to the official version once supported, as it will provide our "pi" the ability to do dns blocking based on ProtonVPN's lists, blocking ads and malware sites as if it were a Pi-Hole. Also it is still currently working with Raspi OS based on Debian 10 (Buster) and 11 (Bullseye).

## ProtonVPN 

We can begin by installing the community app, and doing the initial configuration. The advanced configuration won't be modified:
* **DNS Management**. We'll leave the 1st option, which is also the default and recommended \(Enable DNS Leak Protection\).
* **Kill Switch**. Enabling the Kill Switch will modify the **iptables** rules that will break the routing feature we'll setup later. So leave it disabled (as default).

> I still have to make research to enable the Kill switch manually through **iptables**.

## Routing configuration

This will be achieved by following the guide [https://www.raspberrypi.com/documentation/computers/configuration.html#setting-up-a-routed-wireless-access-point](https://www.raspberrypi.com/documentation/computers/configuration.html#setting-up-a-routed-wireless-access-point).

> The guide uses **MASQUERADING** rule for iptables. I have not accomplished to get the routing working while having a static IP set within the "RPI".
> As said in [Difference between SNAT and MASQUERADE](https://unix.stackexchange.com/questions/21967/difference-between-snat-and-masquerade) **SNAT** is better if we use an static IP, as it  will have lighter overhead. Although I couldn't get it to work setting the static IP on /etc/network/interfaces.

## Troubleshooting

### DNS Leaks

You may notice the clients having DNS leaks. That's because the service **DNSmasq** creates a copy of `/etc/resolv.conf` on boot. You could have the service restarted every time you set a new connection with ProtonVPN, or we could try a couple of tweaks to deal with it.

By reading this [post](https://unix.stackexchange.com/questions/347425/make-dnsmasq-not-altering-resolv-conf), I tried the following:

* Stopping the service with `sudo systemctl stop dnsmasq`

* Adding the following to the config in `/etc/dnsmasq.conf`:
```conf
resolv-file=/etc/resolv.conf
```
  > So it does **not** use the one already processed by the daemon at `/run/dnsmasq/resolv.conf`.

* Commenting out the following lines to the file `/etc/systemd/system/multi-user.target.wants/dnsmasq.service`:
```conf
#ExecStartPost=/etc/init.d/dnsmasq systemd-start-resolvconf
#ExecStop=/etc/init.d/dnsmasq systemd-stop-resolvconf
```
  > This way it the service does not update the `/etc/resolv.conf`

* Executing `sudo systemctl daemon-reload`
----

With all these steps done, the DNS taken into account will be the one set in `/etc/resolv.conf`. That way if we change servers with ProtonVPN, as it uses updates and use that same file, the DNS server will change to the one at the new connection.

> Since now the file taken into account is the one that the ProtonVPN app uses, it will make possible to enable the auto-connect on boot.
> Note that for it to work on boot, it had to initially have a DNS to which query the IP for protonvpn.com
