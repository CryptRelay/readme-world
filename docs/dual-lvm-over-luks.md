# DUAL BOOT WITH LVM OVER LUKS

Having this setup in which we find 2 linux distributions, one root logical volume for each, and another for swap and tmp partitions of each system.

There may be found some issues after making a manual installation, which is the only method available if you want to achieve this setup.

## How to get this setup?

If you have tried the manual installer of some distros, you may find impossible to setup LVM over LUKS from startup.

To do so, we're required to create the logical volumes first outside of the installer. Once that's done we'll be able to use the logical volumes as LUKS volumes.

The idea would be something like: 

LVM:
 \# /dev/vg/lv
* /dev/mypc/root1 as /
* /dev/mypc/tmp1_crypt as /tmp
* /dev/mypc/swap1_crypt as [SWAP]
* /dev/mypc/root2 as /
* /dev/mypc/tmp2_crypt as /tmp
* /dev/mypc/swap2_crypt as [SWAP]

In case the LVs are on different partitions, to see which ones are unlocked on LUKS:
```bash
sudo dmsetup ls --target crypt
```

## Changing LV or VG name of swap or tmp partition on hibernated device

If it is attempted to change the name of the VG or LV for tmp or swap partitions, the system will not find these volumes.
On startup the following will display:
```bash
Begin: Running /scripts/local-block ... Failed to find logical volume /dev/mypc/swap2_crypt
Gave up waiting for suspend/resume device
```

This must be fixed by fixing the corresponding value of the key "RESUME" in **/etc/initramfs-tools/conf.d/resume**
